import './App.css';
import Div from './Div';
import Div2 from './Div2';
import Div3 from './Div3';
import Nav from './Nav';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'
import Subscribe from './Subscribe';

function App() {

  return (
    <Router>
      <div className="app">
        <Switch>
          <Route path = '/subscribe'>
            <Subscribe/>
          </Route>
          <Route path = '/'>
            <Nav/>
            <div id = 'div1'><Div/></div>
            <div id = 'div2'><Div2/></div>
            <div id = 'div3'><Div3/></div>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
